FROM node:16-alpine as builder
WORKDIR '/app'
ARG API_URL
ENV REACT_APP_API_BASE_URL=$API_URL
COPY ./package.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx
ARG API_URL
ENV REACT_APP_API_BASE_URL=$API_URL
EXPOSE 3000
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build /usr/share/nginx/html